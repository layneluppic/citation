//-------- Générateur de citations a citations variables


const quoteCaradoc = [
    "une politique qui court vite, une politique qui fait des gros œufs, c'est tout",
    "oh le con ! Mais il est pas fini d'affiner ",
    "la joie de vivre et le jambon, y'a pas trente-six recettes du bonheur",
    "les chiffres, c'est pas une science exacte figurez-vous",
    "c’est compliqué mais c’est compliqué",
    "le gras, c'est la vie",
];
const quotePerceval = [
    "faut arrêter ces conneries de nord et de sud une fois pour toutes, le nord, suivant comment on est tourné, ça change tout ",
    "c'est pas faux",
    "sire, Sire ! On en a gros",
    "c’est marrant les petits bouts de fromage par terre c’est ça que vous appelez une fondue",
    "mais c'est tout du flan",
];
const quotes = [
   quoteCaradoc,
   quotePerceval,
   [
       ...quoteCaradoc,
       ...quotePerceval,
   ],
];
const separators = innerHTML ="<br/>";
const end = [
    '.',
    '!',
    '?',
];
let result = [];
let resultString = "";


//Elements html
let citation = document.getElementById("citation");
let buttonCaradoc = document.getElementById("genererCaradoc");
let buttonPerceval = document.getElementById("genererPerceval");
let buttonMix = document.getElementById("genererMix");






function getRandomQuote(result, quoteList) {
   let randomQuote = Math.floor(Math.random() * quoteList.length);
   // Si le nombre généré n'existe pas dans le tableau on peut insérer le resultat dans le tableau
   if (result.indexOf(randomQuote) === -1) {
       result.push(randomQuote);
       return randomQuote;
   }
   return getRandomQuote(result, quoteList);
};



function initialize(quoteList) {
   result = [];
   resultString = '';

    const inputRange = document.getElementById("inputrangeQuote");
    



   while (result.length < inputRange.value) {

       if (result.length > 0) {
            for (var i = 0, c = result.length; i < c; i++) {   
            }  
       }
       resultString += '- ';
       resultString += quoteList[getRandomQuote(result, quoteList)];
       resultString += end[Math.floor(Math.random() * end.length)];
       resultString += separators;

       

   }

   resultString = resultString.charAt(0).toUpperCase() + resultString.substring(1).toLowerCase();
   citation.innerHTML = resultString;

};


document.addEventListener("DOMContentLoaded", function() {
  const percevalEventListener = buttonPerceval.addEventListener("click", () => {
      initialize(quotes[1]);
  });
  const caradocEventListener = buttonCaradoc.addEventListener("click", () => {
      initialize(quotes[0]);
  });
  const mixEventListener = buttonMix.addEventListener("click", () => {
     initialize(quotes[2]);
  });
});

//---------- Design du site 

let caradocItem = document.getElementById("item--listC");
let percevalcItem = document.getElementById("item--listP");
let mixItem = document.getElementById("item--listB");

$('.item--list').click(function() {
    $('.item--list').addClass('minWidth');
    $('.item--list').removeClass('maxWidth');
    $(this).removeClass('minWidth');
    $(this).addClass('maxWidth');
});

var loader = document.querySelector(".loader");

window.addEventListener("load", function() {
  
    setTimeout(function(){
      loader.classList.add("isloaded");
      }, 3000);
    setTimeout(function(){
    loader.classList.add("hidden");
      }, 4200);
});
function myFunction() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}